variable "prefix" {
  default = "raad"
}

variable "project" {
  default = "recipe-app-api-devops"
}

variable "contact" {
  default = "690318347@qq.com"
}

variable "db_username" {
  description = "Username for the RDS postgres instance"
}

variable "db_password" {
  description = "Password for the RDS postgres instance"
}

variable "bastion_key_name" {
  default = "recipe-app-api-devops-bastion"
}

variable "ecr_image_api" {
  description = "ecr image for api"
  default     = "691014198162.dkr.ecr.cn-north-1.amazonaws.com.cn/recipe-app-api-devops:latest"
}

variable "ecr_image_proxy" {
  description = "ecr image for proxy"
  default     = "691014198162.dkr.ecr.cn-north-1.amazonaws.com.cn/recipe-app-api-proxy:latest"
}

variable "django_secret_key" {
  description = "secret key for django app"
  #var set in project
}